class PermaLink {

    // method to update the url and add it to lien permanent button 
    setLink(options) {
        var q = '?';
        if (options.start)
            q += '&ds=' + options.start;
        if (options.end)
            q += '&de=' + options.end;
        if (options.compare)
            q += '&comp=' + options.compare;
        if (options.meteo.t)
            q += '&mt=1';
        if (options.meteo.rr3)
            q += '&mrr3=1';
        if (options.selectedCounters.length > 0)
            options.selectedCounters.forEach(function (item) {
                q += '&c=' + item;
            });
        if (options.sunday)
            q += '&s=1';
        q += '&m=' + encodeURIComponent(options.mapBaseLayer);

        var url = window.location.href.split('?')[0] + q

        $('#permalink').prop('href', url); // add the link to lien permanent button

        window.history.replaceState(history.state, "", url) // update the url 
    }

    // method to git the args from url and set them in the page
    optionsParse(options, url) {
        var hasArg = false;
        var q = url;
        if (!q || q.length == 0)
            return hasArg; // return false if the url has no arg
        if (q[0] == '?')
            q = q.substring(1);
        var args = q.split('&');
        args.forEach(function (item) {
            hasArg = true;
            var arg = item.split('=');
            switch (arg[0]) {
                case 'ds': options.start = moment(arg[1]); break;
                case 'de': options.end = moment(arg[1]); break;
                case 'comp': options.compare = arg[1]; break;
                case 'mt': options.meteo.t = arg[1] ? true : false; break;
                case 'mrr3': options.meteo.rr3 = arg[1] ? true : false; break;
                case 'c': options.selectedCounters.push(arg[1]); break;
                case 's': options.sunday = arg[1] ? true : false; break;
                case 'm': options.mapBaseLayer = decodeURIComponent(arg[1]); break;
            }
        });
        return hasArg; // return true if the url has arg
    }
}