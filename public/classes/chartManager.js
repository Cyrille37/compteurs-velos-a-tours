/**chartManager classe with 
 * three propertya (chart, chartWeek, chartTotal) 
 * three method for init the charts (chartInit, chartWeekInit, chartTotalInit)
 * three method for update the charts (chartUpdate, chartWeekUpdate,chartTotalUpdate)
 * one method for chartReload
*/
class ChartManager {
    constructor() {
        this.chart
        this.chartWeek
        this.chartTotal
        this.countersUtil = {

            /**
             * Change point size on sunday or over.
             */
            pointRadius: (item) => {
                if (item.active)
                    return 6;
                // When no data, this function is still called :-(
                if (item.dataset.data[item.dataIndex]) {
                    var x = item.dataset.data[item.dataIndex].x;
                    if (options.sunday && x.isoWeekday() == 7)
                        return 6;
                }
                return 0;
            },
            /**
             * Change point shape on sunday
             */
            pointStyle: (item) => {
                if (item.active || (!options.sunday))
                    return 'circle';
                return 'star';
            },
        };
    }
    chartInit() {
        this.chart = new Chart(jQuery('#counter-chart')[0].getContext('2d'),
            {
                type: 'line',
                options: {
                    responsive: true,
                    radius: 0, borderWidth: 2,
                    interaction: {
                        mode: 'nearest',
                        axis: 'x',
                        intersect: false,
                    },
                    scales: {
                        x: {
                            type: 'time',
                            time: {
                                // https://www.chartjs.org/docs/2.9.4/axes/cartesian/time.html#parser
                                displayFormats: {
                                    quarter: 'YYYY',
                                    month: 'MMM YYYY',
                                    day: 'ddd DD/MM/YY',
                                    hour: 'ddd DD/MM/YY',
                                    minute: 'HH:mm:ss',
                                    second: 'HH:mm:ss'
                                },
                                tooltipFormat: 'dddd DD/MM/YYYY',
                            },
                            ticks: {
                                // source: 'data'
                            },
                        },
                        y: {
                            display: true,
                            type: 'linear',
                            title: {
                                display: true,
                                text: 'Compteur'
                            },
                        },
                        yTemp: {
                            display: true, drawOnChartArea: false,
                            type: 'linear',
                            position: 'right',
                            title: {
                                display: true,
                                text: 'Température'
                            },
                        },
                        yPrecip: {
                            display: true,
                            drawOnChartArea: false,
                            type: 'linear',
                            position: 'right',
                            title: {
                                display: true,
                                text: 'Précipitations'
                            },
                        },
                    },
                    plugins: {
                        title: {
                            display: true,
                            text: 'Courbes des compteurs',
                            padding: { top: 8, bottom: 0 },
                        },
                        legend: {
                            labels: {
                                filter: function (item) {
                                    // Pas de data, pas de légende
                                    if (self.chart?.data.datasets[item.datasetIndex].data.length == 0)
                                        return false;

                                    // Customize meteo legend items
                                    switch (item.datasetIndex) {
                                        case meteoDatasetsIdx['t'].idx:
                                        case meteoDatasetsIdx['rr3'].idx:
                                            item.fillStyle = 'white';
                                            item.lineWidth = 3;
                                            break;
                                    }
                                    return item;
                                }
                            }
                        },
                    },
                },
                data: {
                    datasets: [
                    ],
                },
            });
        meteoDatasetsIdx['t'].idx = this.chart.data.datasets.length;
        this.chart.data.datasets.push({
            data: [],
            label: 'temperature',
            borderColor: '#F40', backgroundColor: '#F40',
            borderDash: [4, 8],
            borderWidth: 1,
            tension: 0.4,
            yAxisID: 'yTemp'
        });
        meteoDatasetsIdx['rr3'].idx = this.chart.data.datasets.length;
        this.chart.data.datasets.push({
            data: [],
            label: 'précipitations',
            borderColor: '#44F', backgroundColor: '#44F',
            borderDash: [4, 8],
            borderWidth: 1,
            tension: 0.4,
            yAxisID: 'yPrecip'
        });
    }
    chartWeekInit() {
        this.chartWeek = new Chart(
            $('#week-chart-col .chart')[0].getContext('2d'),
            {
                type: 'radar',
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    plugins: {
                        title: {
                            display: true,
                            text: 'Total par jour',
                            padding: { top: 8, bottom: 0 },
                        },
                        subtitle: {
                            display: true,
                            text: 'date start, date end',
                            padding: { bottom: 10 },
                        },
                        legend: {
                            display: true,
                            position: 'bottom',
                        },
                    },
                },
                data: {
                    labels: [
                        'Lundi',
                        'Mardi',
                        'Mercredi',
                        'Jeudi',
                        'Vendredi',
                        'Samedi',
                        'Dimanche'
                    ],
                    datasets: []
                }
            });
    }
    chartTotalInit() {
        this.chartTotal = new Chart($('#total-chart-col .chart')[0].getContext('2d'),
            {
                type: 'bar',
                plugins: [ChartDataLabels],
                data: null,
                options: {
                    responsive: true,
                    maintainAspectRatio: true,
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    },
                    plugins: {
                        tooltip: { enabled: false },
                        legend: {
                            display: false,
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Total des compteurs',
                            padding: { top: 8, bottom: 2 },
                        },
                        subtitle: {
                            display: true,
                            text: 'date start, date end',
                            padding: { bottom: 20 },
                        },
                        datalabels: {
                            anchor: 'end', // remove this line to get label in middle of the bar
                            align: 'end',
                            font: { weight: 'bold' },
                            color: 'black',
                            formatter: (val) => (new Intl.NumberFormat().format(val)),
                        }
                    }
                },
            });

    }
    chartUpdate() {
        // Fill chart datasets with counters[].data

        this.chart.options.plugins.title.text =
            moment(options.start).format('LL')
            + ' -> '
            + moment(options.end).format('LL');

        /**
        * Mémorise l'index du dataset (données du graphique)
        * pour chaque compteur.
        */
        var datasetsIndex = {};
        var datasetsIdxOffset = Object.keys(meteoDatasetsIdx).length;
        for (var selectedCounterIdx in options.selectedCounters) {

            var counterId = options.selectedCounters[selectedCounterIdx];
            var counter = counters[counterId];
            var datasetIdx;

            if (datasetsIndex[counterId] != undefined) {
                // Retrouve le Dataset de ce compteur
                datasetIdx = datasetsIndex[counterId].idx;
            }
            else {
                // Créer un Dataset pour ce compteur
                // Index du Dataset
                datasetIdx = this.chart.data.datasets.length;
                // Mémorise l'index du Dataset pour ce compteur
                datasetsIndex[counterId] = {
                    idx: datasetIdx,
                    min: 0, max: 0, // used for "form mode"
                };
                var color = colors[(datasetIdx - datasetsIdxOffset) % colors.length]
                this.chart.data.datasets.push({
                    counter_id: counterId,
                    data: [],
                    label: counter.record.nom_compteur,
                    borderColor: color, backgroundColor: color,
                    tension: 0.4,
                    pointRadius: this.countersUtil.pointRadius,
                    pointStyle: this.countersUtil.pointStyle,
                });
                // photo
                var counter = counters[counterId];
                if (counter.data.length > 0)
                    counter.record.photourl = counter.data[0].photourl;
                dataManager.photoLoad(counter);
            }

            var k = 100.0 / counter.max;
            for (var idx in counter.data) {

                if (options.compare == 'forms') {
                    // Recompute datasets to make values between 0 to 100.

                    this.chart.data.datasets[datasetIdx].data.push({
                        x: counter.data[idx].date.startOf('day'),
                        y: k * counter.data[idx].counts,
                    });

                }
                else {
                    // Ajoute les données dans le Dataset du compteur
                    this.chart.data.datasets[datasetIdx].data.push({
                        x: counter.data[idx].date.startOf('day'),
                        y: counter.data[idx].counts,
                    });
                }
            }

        } // in options.selectedCounters

        this.chartTotalUpdate(options, colors);
        this.chartWeekUpdate(options, colors)
        // chartWeekUpdate(options, colors);

    }
    chartWeekUpdate() {
        if (!this.chartWeek) {
            this.chartWeekInit()
        }
        this.chartWeek.options.plugins.subtitle.text =
            moment(options.start).format('LL')
            + ' -> '
            + moment(options.end).format('LL');

        this.chartWeek.data.datasets = [];
        var c, color;
        for (var idx in options.selectedCounters) {
            c = counters[options.selectedCounters[idx]];
            color = colors[this.chartWeek.data.datasets.length % colors.length];

            var days;
            if (options.compare == 'forms') {

                var max = 0;
                c.week.forEach((v) => max = Math.max(max, v));
                var k = 100.0 / max;
                days = [];
                c.week.forEach((v) => days.push(Math.round(k * v)));
            }
            else {
                days = c.week;
            }
            this.chartWeek.data.datasets.push({
                label: c.record.nom_compteur,
                data: days,
                borderColor: color,
                fill: false,
            });

        }
        this.chartWeek.update()

    }
    chartTotalUpdate() {
        if (!this.chartTotal) {
            this.chartTotalInit()
        }
        this.chartTotal.options.plugins.subtitle.text =
            moment(options.start).format('LL')
            + ' -> '
            + moment(options.end).format('LL');

        this.chartTotal.data = {
            labels: [],
            datasets: [{
                data: [],
                borderWidth: 3,
                borderColor: [],
                backgroundColor: [],
            }],
        };
        var ds = this.chartTotal.data.datasets[0];

        var c, color;
        for (var idx in options.selectedCounters) {
            c = counters[options.selectedCounters[idx]];
            color = colors[ds.data.length % colors.length];
            this.chartTotal.data.labels.push(c.record.nom_compteur);
            ds.data.push(c.total);
            ds.borderColor.push(color);
            ds.backgroundColor.push(color + 'BF');
        }


        this.chartTotal.update();
    }

    chartReload() {
        //console.debug('chartReload()');
        const chartThis = this
        if (!this.chart)
            return;

        $('#loader').show();
        permaLink.setLink(options)

        // Without this setting the graph is automatically adjusted to the data,
        // which is disturbing...
        this.chart.options.scales['x'].suggestedMin = options.start;
        this.chart.options.scales['x'].suggestedMax = options.end;

        // options.compare
        // display=true car avec 'auto' le graph vide n'est pas affiché quand pas de compteur
        this.chart.options.scales.y.display = (options.compare == 'forms' ? false : true);
        // options.meteo
        this.chart.options.scales.yTemp.display = (options.meteo.t ? 'auto' : false);
        this.chart.options.scales.yPrecip.display = (options.meteo.rr3 ? 'auto' : false);

        // select box has at least one counter selected
        /*if (options.selectedCounters.length == 0 ) {
            chart.update();
            return;
        }*/

        // Supprime tous les datasets des compteurs,
        // c'est à dire ceux après les datasets météo

        //var meteoDsCount = Object.keys(meteoDatasetsIdx).length ;
        var i = 0;
        for (var k in meteoDatasetsIdx) {
            this.chart.data.datasets[meteoDatasetsIdx[k].idx].data = [];
            i++;
        };
        this.chart.data.datasets.splice(i);

        // Promise pour le chargement des données via les API
        // (ou pas si on a déjà les données).
        $.when(dataManager.loadTmvlOD(), dataManager.loadMeteo())
            .then(function () {
                // Les chargements sont terminés
                chartThis.chart.update();
                $('#loader').hide();
            });

    }
}




