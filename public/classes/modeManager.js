// class with three method for init the modes

class ModeManager {

    compareModeInit() {

        var $block = $('#compare-mode');

        if (options.compare && options.compare == 'forms') {
            $('input[value="forms"]', $block).prop('checked', true);
        }
        else {
            $('input[value="values"]', $block).prop('checked', true);
            options.compare = 'values';
        }

        $block.on('change', function () {
            options.compare = $('input:checked', $block).val();
            chartManager.chartReload();
        });
    }
    meteoModeInit() {

        var $block = $('#meteo-mode');

        $('#meteo-mode-temp', $block).prop('checked', options.meteo.t);
        $('#meteo-mode-precip', $block).prop('checked', options.meteo.rr3);

        $block.on('change', function () {

            options.meteo.t = $('#meteo-mode-temp', $block).prop('checked');
            options.meteo.rr3 = $('#meteo-mode-precip', $block).prop('checked');

            chartManager.chartReload();
        });
    }
    otherModeInit() {
        var $block = $('#other-mode');

        if (options.sunday) {
            $('#other-mode-sunday', $block).prop('checked', true);
        }

        $block.on('change', function () {
            options.sunday = $('#other-mode-sunday', $block).prop('checked');
            //chartReload();
            chartManager.chart.update();
            permaLink.setLink(options)
        });

    }
}