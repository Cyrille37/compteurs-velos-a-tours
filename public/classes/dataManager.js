class DataManager {
    constructor() {
        this.TmvlODApi = 'https://data.tours-metropole.fr/api';
        this.MeteoApi = 'https://meteo.comptoir.net/api';
    }
    loadMeteo() {

        var meteoData = [];
        if (options.meteo.t)
            meteoData.push('t');
        if (options.meteo.rr3)
            meteoData.push('rr3');

        if (!meteoData.length)
            return;

        var station_omm = 7240;
        var q = this.MeteoApi
            + '/synops'
            + '/' + station_omm
            + '/' + options.start
            + '/' + options.end
            + '?fields=measured_at,' + meteoData.join(',')
            ;

        return jQuery.get(q, function (result) {

            // Fill datasets data
            for (var idx in result.measures) {
                var measure = result.measures[idx];

                var d = new Date(measure.measured_at);
                //console.debug(d, d.getHours());
                if (d.getHours() != 12)
                    continue;

                // Ajoute les données dans le Dataset du compteur
                for (var idx in meteoData) {
                    var k = meteoData[idx];
                    var value = measure[k];
                    switch (k) {
                        case 't':
                            value = value - 273.15;
                            break;
                        case 'rr3':
                            value = value < 0 ? 0 : value;
                            break;
                    }
                    chartManager.chart.data.datasets[meteoDatasetsIdx[k].idx].data.push({
                        x: moment(measure.measured_at).startOf('day'),
                        y: value,
                    });
                }
            }
        });
    }
    loadTmvlOD() {

        if (options.selectedCounters.length == 0 || Object.keys(counters).length == 0)
            return;

        // Clear the photos list
        this.photoLoad();

        // API hard limit to 10000
        var rowsCount = 10000;

        /**
        https://data.tours-metropole.fr/api/records/1.0/search/
            ?dataset=comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine
            &refine.id=300017599
            &refine.id=100063142
            &disjunctive.id=true
            &facet=date
            &q=date:[2022-08-16T22:00:00Z%20TO%202022-08-18T22:00:00Z]
            &sort=date
            &rows=5000
            &fields=id,counts,sens,nom_compteur,photourl,date
        */
        var q = this.TmvlODApi + '/records/1.0/search/'
            + '?dataset=comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine'
            + '&timezone=UTC'
            ;

        var needQuery = false;
        for (var idx in options.selectedCounters) {
            // Ajoute à la requête les compteurs sélectionnés
            // Si un compteur n'a pas de données, hop !
            if ((!counters[options.selectedCounters[idx]].data) || (counters[options.selectedCounters[idx]].data.length == 0)) {
                var c = counters[options.selectedCounters[idx]];
                c.data = [];
                c.min = Number.MAX_SAFE_INTEGER;
                c.max = 0;
                c.total = 0;
                c.week = [0, 0, 0, 0, 0, 0, 0];
                q += '&refine.id=' + options.selectedCounters[idx];
                needQuery = true;
            }
        }

        if (needQuery) {
            // https://help.opendatasoft.com/apis/ods-search-v1/#refining
            q += '&disjunctive.id=true';
            q += '&facet=date&q=date:['
                + tmvlODdateFormat(options.start)
                + ' TO '
                + tmvlODdateFormat(options.end)
                + ']';
            q += '&sort=date';
            q += '&rows=' + rowsCount;
            q += '&fields=id,counts,sens,nom_compteur,photourl,date';

            return jQuery.get(q, function (result) {

                //console.debug( 'TmvlOD results count:', result.records.length );

                // Update counters[x].data ;
                for (var idx in result.records) {
                    var record = result.records[idx].fields;
                    var c = counters[record.id];
                    record.date = moment(record.date);
                    c.data.push(record);
                    c.min = Math.min(c.min, record.counts);
                    c.max = Math.max(c.max, record.counts);
                    c.total += record.counts;
                    var day = record.date.isoWeekday();
                    c.week[day - 1] += record.counts;
                }

                chartManager.chartUpdate();
            });
        }
        else {
            chartManager.chartUpdate();

        }
        function tmvlODdateFormat(d) {
            if (typeof d == 'string')
                d = moment(d);
            return d.toISOString().replace(/[.]\d+/, '');
        }



    }// loadTmvlOD()

    datePickerInit() {

        var $datespicker = $('#datespicker');

        var start = options.start ? moment(options.start) : moment().subtract(30, 'days'),
            end = options.end ? moment(options.end) : moment();

        function dpUpdate(start, end) {

            $('span', $datespicker).html(start.format('LL') + ' - ' + end.format('LL'));

            options.start = start.format('YYYY-MM-DD');
            options.end = end.format('YYYY-MM-DD');

            // Force reload of counters data.
            for (var id in counters) {
                counters[id].data = [];
            }
            chartManager.chartReload();

        }

        $datespicker.daterangepicker({
            startDate: start,
            endDate: end,
            maxDate: moment().endOf('days'),
            showISOWeekNumbers: true,
            showDropdowns: true,
            ranges: {
                '7 jours': [moment().subtract(7, 'days'), moment()],
                '15 jours': [moment().subtract(15, 'days'), moment()],
                '1 mois': [moment().subtract(1, 'months'), moment()],
                '3 mois': [moment().subtract(3, 'months'), moment()],
                '6 mois': [moment().subtract(6, 'months'), moment()],
                '1 an': [moment().subtract(1, 'years'), moment()],
                '3 ans': [moment().subtract(3, 'years'), moment()]
            },
            locale: {
                weekLabel: "S",
                cancelLabel: 'Annuler', applyLabel: "Appliquer",
                customRangeLabel: "Choisir la période",
            },
        }, dpUpdate);

        dpUpdate(start, end);
    }
    /**
     * Manage #photos list:
     * - without parameter to clear the list.
     * - with a counter object to add its photo.
     */
    photoLoad(counter) {

        if (counter === undefined) {
            $('#photos').html('');
            return;
        }

        var html = '';
        html += '<div class="col-3 text-center">';
        html += '<img class="img-thumbnail h-75" src="'
            // if null don't write a "null" ;-)
            + (counter.record.photourl ? counter.record.photourl : '')
            + '" alt="' + counter.record.nom_compteur
            + '" title="' + counter.record.nom_compteur
            + '" onerror="this.onerror=null;this.src=\'https://cyrille.giquello.fr/foobar/not-found-macintosh.jpg\'" />';
        html += '<small class="text-white bg-secondary"><br/>' + counter.record.nom_compteur + '</small>';
        html += '</div>';
        $('#photos').append(html);
    }
}