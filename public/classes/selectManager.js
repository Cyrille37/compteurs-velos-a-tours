class SelectManager {

    selectInit() {

        $('#loader').show();

        var $select = jQuery('#counters-select');

        var q = dataManager.TmvlODApi + '/v2/catalog/datasets/comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine/records'
            + '?select=%2A'
            + '&group_by=nom_compteur%2Cid%2Ccoordinates'
            + '&limit=200'
            + '&order_by=nom_compteur'
            + '&offset=0'
            + '&timezone=UTC'
            ;

        counters = {};

        jQuery.get(q, function (result) {

            result.records.forEach((item) => {
                var record = item.record.fields;

                if (countersBlackList.find(item => item == record.id))
                    return;

                // potential duplicate
                // @see https://framagit.org/Cyrille37/compteurs-velos-a-tours/-/issues/3
                //if( counters.find( item => item.id == record.id ) )
                //	return ;
                if (counters[record.id])
                    return;

                // coordinates is a string :(
                record.coordinates = JSON.parse(item.record.fields.coordinates);
                // Cannot get photourl with this query because:
                // - there are several photos
                // - and the field must be in "group_by"
                // @see https://framagit.org/Cyrille37/compteurs-velos-a-tours/-/issues/3
                // record.record.photourl = null ;

                counters[record.id] = {
                    record: record
                };

                //var disabled = countersBlackList.find( item => item == record.id )
                var selected = options.selectedCounters.find(item => item == record.id);
                $select
                    .append('<option value="' + record.id + '"'
                        //+ ( disabled ? ' disabled' : '')
                        + (selected ? ' selected' : '')
                        + '>' + record.nom_compteur + '</option>');
            });

            $select.on('change', () => {
                //console.debug('select.onChange()');
                options.selectedCounters = $select.val();
                if (!options.selectedCounters)
                    options.selectedCounters = [];

                mapManager.mapMarkersRefresh();
                chartManager.chartReload();
            });

            // retaille la hauteur de la select box
            var n = Object.keys(counters).length;
            $select.prop('size', n);
            $('#counters-count').html(n);

            mapManager.mapMarkersInit();

            $select.trigger('change');

            $('#loader').hide();
        });

    }
    selectCounter(record_id, show) {
        //console.debug('selectCounter',record_id, show);
        let record = counters[record_id].record;

        let html = record.nom_compteur + '<br/>';
        if (show) {
            html += '<button class="btn btn-sm btn-outline-primary" onclick="selectCounter(' + record_id + ', false)">retirer</button>';
            options.selectedCounters.push(record_id);
            var $select = jQuery('#counters-select option[value="' + record_id + '"]').prop('selected', true);
        }
        else {
            html += '<button class="btn btn-sm btn-outline-primary" onclick="selectCounter(' + record_id + ', true)">afficher</button>';
            options.selectedCounters.splice(options.selectedCounters.indexOf(record_id), 1);
            var $select = jQuery('#counters-select option[value="' + record_id + '"]').prop('selected', false);
        }

        // Cannot make content updated :-(
        mapManager.markerPopup.setContent(html);
        mapManager.markerPopup.update();
        const b = $(mapManager.markerPopup.getElement());

        b.outerHtml = html;

        mapManager.markerPopup.close();

        mapManager.mapMarkersRefresh();
        chartManager.chartReload();


    }

}