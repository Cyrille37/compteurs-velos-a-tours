/**
 * mapManager class with:
 * three propertys (map, mapLayer, markerPopup)
 * three method (mapInit, mapMarkersInit, mapMarkersRefrech)
*/
class MapManager {
    constructor() {
        this.map
        this.mapLayer
        this.markerPopup
    }
    mapInit() {

        this.map = L.map('map', {
            scrollWheelZoom: false,
        }).setView([47.3903, 0.6889], 10);

        const baseLayers = {
            'OpenStreetMap': L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '© OpenStreetMap'
            }),
            'Osm HOT': L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '© OpenStreetMap contributors, Tiles style by Humanitarian OpenStreetMap Team hosted by OpenStreetMap France'
            }),
            'CyclOSM': L.tileLayer('https://{s}.tile-cyclosm.openstreetmap.fr/cyclosm/{z}/{x}/{y}.png', {
                attribution: 'Map &copy; : Data <a href="https://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap contributors</a>, Style <a href="https://www.cyclosm.org/" target="_blank">CyclOSM</a>, Power <a href="https://openstreetmap.fr/">OpenStreetMap France</a>. ',
                subdomains: 'abc',
                maxZoom: 20,
            }),
        };
        L.control.layers(baseLayers).addTo(this.map);

        if (!baseLayers.hasOwnProperty(options.mapBaseLayer))
            // Default base layer
            options.mapBaseLayer = 'CyclOSM';

        this.map.addLayer(baseLayers[options.mapBaseLayer]);

        this.map.on('baselayerchange', function (e) {
            options.mapBaseLayer = e.name;
            permaLink.setLink(options)

        });
    }
    mapMarkersInit() {
        const mapThis = this // in event this = eventObj
        if (this.mapLayer)
            this.mapLayer.clearLayers();
        else {
            this.mapLayer = L.featureGroup();
        }

        var bicycleIcon = L.icon({
            //iconUrl: 'https://twemoji.maxcdn.com/2/svg/1f6b2.svg',
            iconUrl: 'https://unpkg.com/twemoji@2.0.0/2/svg/1f6b2.svg',
            //shadowUrl: 'leaf-shadow.png',
            iconSize: [38, 38],
            iconAnchor: [20, 32],
            popupAnchor: [0, -15]
        });

        if (!this.markerPopup)
            this.markerPopup = L.popup();


        var c;
        for (var idx in counters) {
            c = counters[idx];
            //console.debug(c);
            c.marker = L.marker(
                [c.record.coordinates[0], c.record.coordinates[1]],
                { icon: bicycleIcon, record: c.record }
            )
                //.bindPopup(c.record.nom_compteur)
                .on('click', function (ev) {
                    const record = ev.sourceTarget.options.record;
                    const selected = options.selectedCounters.find(item => item == record.id);
                    let html = '<p><b>' + record.nom_compteur + '</b></p>';
                    if (selected) {
                        html += '<button class="btn btn-sm btn-outline-primary" onclick="selectManager.selectCounter(' + record.id + ', false)">retirer</button>';
                    }
                    else {
                        html += '<button class="btn btn-sm btn-outline-primary" onclick="selectManager.selectCounter(' + record.id + ', true)">afficher</button>';
                    }
                    mapThis.markerPopup.setLatLng(ev.latlng).setContent(html).openOn(mapThis.map);

                });
            ;
            this.mapLayer.addLayer(
                c.marker
            );
        }

        this.mapLayer.addTo(this.map);
        this.map.fitBounds(this.mapLayer.getBounds().pad(0.4));
    }
    mapMarkersRefresh() {

        // At least one counter
        if (options.selectedCounters.length == 0)
            return;

        // Remove class "selected" to change icon's color
        this.mapLayer.eachLayer(function (marker) {
            marker._icon.classList.remove('selected');
        });

        for (var idx in options.selectedCounters) {
            var c = counters[options.selectedCounters[idx]];
            // Add class "selected" to change icon's color
            c.marker._icon.classList.add('selected');
        }
    }
}