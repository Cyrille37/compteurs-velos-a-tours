/**
 * 
 */
"use strict";

/**
 * Quelques variables "globales".
 */

/**
 * Url pour les données en Open Date des compteurs vélos de Tours-Métropole
 * Doc des API:
 * https://help.opendatasoft.com/en/apis/
 */

var colors = [
	'#f53794',
	'#acc236',
	'#537bc4',
	'#e74c3c',
	'#00a950',
	'#af7ac5',
	'#f67019',
	'#4dc9f6',
	'#166a8f',
	'#8549ba',
	'#58595b',
];

var options = {
	/**
	 * @var Array Selected counters id
	 */
	selectedCounters: [],
	/**
	 * @var boolean Display synday or not
	 */
	sunday: false,
	/**
	 * @var moment Begin of dates range
	 */
	start: null,
	/**
	 * @var moment End of dates range
	 */
	end: null,
	/**
	 * @var string Compare "values" or "forms"
	 */
	compare: 'values',
	/**
	 * @var Object Meteo data dysplay
	 */
	meteo: {
		/**
		 * @var boolean Display temperature
		 */
		t: false,
		/**
		 * @var boolean Display precipitations (rain)
		 */
		rr3: false,
	},
	mapBaseLayer: null,

};

/**
 * Do not display those counters
 */
var countersBlackList = [
	300017557, // Pont Arcole, 1 seule mesure
	300016165, // pont st sauveur oues, 1 seule mesure
	// 100039161, // Passerelle Fournier, 709 mesures du 05/07/2017 au 13/06/2019
	100030432, // Pont d'Arcole #1, 400 mesures du 02/06/2016 au 06/07/2017 qui sont déjà contenues dans Pont d'Arcole #2

];

// Stocke les metadonnées des compteurs
var counters = {
	/*
	counter_id : {
		marker : the map L.marker,
		record : toutes les propriétés reçues via l'api
	}
	*/
};

/**
 * Les datasets pour la météo sont créé une fois pour toute
 * dans chartInit(), alors que les datasets pour les compteurs sont
 * créés à chaque chartReload().
 */
var meteoDatasetsIdx = {
	't': { idx: null },
	'rr3': { idx: null }
};


// ********** start classes **********

const dataManager = new DataManager()

const permaLink = new PermaLink()

const chartManager = new ChartManager()

const mapManager = new MapManager()

const selectManager = new SelectManager()

const modeManager = new ModeManager()

// ********** end classes **********
/**
 * Démarrage de l'application
*/
jQuery(function () {

	moment.locale('fr');

	options.start = moment().subtract(1, 'months').format('YYYY-MM-DD');
	options.end = moment().format('YYYY-MM-DD');
	if (!permaLink.optionsParse(options, window.location.search)) {
		$('#info-modal').modal('show');
	}
	$('header .fa-question-circle').on('click', function () {
		$('#info-modal').modal('show');
	});

	chartManager.chartInit();
	mapManager.mapInit();

	modeManager.otherModeInit();
	dataManager.datePickerInit();
	modeManager.compareModeInit();
	modeManager.meteoModeInit();
	clipboard()
	// must be last
	selectManager.selectInit();

	// Chart's zoom
	fullWindowInit(["#counter-chart-row", "#total-chart-col", "#week-chart-col"]);
});

function fullWindowInit(chartsId) {
	chartsId.forEach(chartId => {

		const $buttonZoom = $(`${chartId} button.unzoom`)
		const $buttonUnzoom = $(`${chartId} button.zoom`)
		const $wrapper = $(`${chartId} .wrapper`)

		// Zoom button

		$buttonZoom.on('click', zoom).show();
		$buttonUnzoom.on('click', zoom).hide();
	})
	function zoom() {
		if ($wrapper.css('position') == 'fixed') {
			$buttonZoom.show();
			$buttonUnzoom.hide();
			$wrapper.css($wrapper.data('css-backup'));
		}
		else {
			$buttonZoom.hide();
			$buttonUnzoom.show();
			$wrapper.data('css-backup',
				$wrapper.css(
					['position', 'top', 'left', 'right', 'bottom', 'height', 'width', 'background-color', 'z-index']
				)
			);
			$wrapper.css({
				position: 'fixed',
				top: 0, left: 0,
				bottom: 0, right: 0,
				height: "100vh", width: "100vw",
				"background-color": "white",
				"z-index": 9999
			});
		}
	};
}


function clipboard() {

	$(".copy-lien")[0].addEventListener('click', function () {
		// Trigger the copied state
		$(".clippy-icon")[0].style.strokeDashoffset = '-50';
		$(".check-icon")[0].style.strokeDashoffset = '0';

		// Reset the icons after 1 second
		setTimeout(() => {
			$(".clippy-icon")[0].style.strokeDashoffset = '0';
			$(".check-icon")[0].style.strokeDashoffset = '-50';
		}, 2000);

		navigator.clipboard.writeText(window.location.href).catch(err => {
			console.error(err)
			$(".copy-lien")[0].style = "display:none"
		});
	});

	if ('clipboard' in navigator) {
		// API prensents
	}
	else
	{
		$(".copy-lien")[0].style = "display:none" ;
	}

};

