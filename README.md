# Compteurs vélos à tours

Visualisation des Compteurs Vélos de Tours Métropole.

https://cyrille37.frama.io/compteurs-velos-a-tours/

![screenshot](<./public/screenshot.png>) 

Le contenu de ce projet est partagé selon les conditions de la licence GPL v3.

Voir aussi le projet de [Guillaume](https://gitlab.com/gdebrion/compteur-velos-tours/-/tree/master) qui tweete chaque jours quelques compteurs sur [@CyclistesATours](https://twitter.com/CyclistesATours).

## API tips

### refining

Selon la documentation de [refining](https://help.opendatasoft.com/apis/ods-search-v1/#refining) on peut filtrer sur plusieurs valeurs pour le même champ. Il faut disjoindre les facets avec `disjunctive.<facet>`.

Pour chercher sur 2 compteurs par leur `id` il faut construire la requête avec:
- refine.id=100030433
- refine.id=300022520
- disjunctive.id=true

Pour le "Pont de fil (100030433)" et le "Pont Wilson TOTAL (300022520)" sur le mois d'Avril il faut:

https://data.tours-metropole.fr/api/records/1.0/search/?dataset=comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine&q=date%3A[2022-03-31T22%3A00%3A00Z+TO+2022-04-28T21%3A59%3A59Z]&refine.id=100030433&refine.id=300022520&disjunctive.id=true

### Date

- pas de milliseconde
- ni timezone offset, seulement UTC avec "Z".
